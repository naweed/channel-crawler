var Post = require('./models/post/post');
var sync = require('synchronize');
var persist = require('./crawlerPersistance');

var log4js = require('log4js');
log4js.configure({
    appenders: {
        channelFinder: { type: 'file', filename: 'system.log' }
    },
    categories: {
        default: { appenders: [ 'channelFinder' ], level: 'debug' }
    }
});
var logger = log4js.getLogger();



function DbBatcher(projection, limit, lastID) {
    logger.info('building a new post batcher ...');
    this.projection = projection;
    this.limit = limit;
    this.lastDocID = lastID;
}


DbBatcher.prototype.nextBatch = function (callback) {
    var query = {};

    if (this.lastDocID) {
        query = {_id: {$gt: this.lastDocID}};
    }

    var thisObj = this;

    sync(Post, 'find');
    sync.fiber(function () {
        var docs;
        try {
            docs = Post.find(query, thisObj.projection, {limit: thisObj.limit});
            if(docs.length > 0) {
                thisObj.lastDocID = docs[docs.length - 1]._id;
                persist.submitLastKey(thisObj.lastDocID.toString());
            }
            callback(null, docs);
        } catch (err) {
            logger.error('cant retrieve next batch! ' + err.toString());
            callback(err, null);
        }
    });

};

module.exports = DbBatcher;