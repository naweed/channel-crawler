var redis = require('redis');
const config = require('../config');
var client = redis.createClient();
var log4js = require('log4js');

log4js.configure({
    appenders: {
        channelFinder: { type: 'file', filename: 'system.log' }
    },
    categories: {
        default: { appenders: [ 'channelFinder' ], level: 'debug' }
    }
});

var logger = log4js.getLogger();
const KEY_PREFIX = 'chusername.';
const LAST_KEY = 'lastkey';
const LAST_TAKEN = 'lastTaken';
const LAST_COUNTER = 'lastCounter';


var counter;
var taken;



client.select(config.redisIndex, function (err) {
    if(err)
        logger.error('couldn\'t get redis index ' + config.redisIndex);
    else {
        client.get(LAST_COUNTER, function (err, res) {
            if(res)
                counter = res;
            else
                counter = 1;
            logger.info('starting redis client with counter: ' + counter);
        });

        client.get(LAST_TAKEN, function (err, res) {
            if(res)
                taken = res;
            else
                taken = 1;
            logger.info('starting redis client with taken: ' + taken);
        });
    }
});

client.on('error', function (err) {
    logger.error('redis countered error : ' + err.toString());
});



module.exports.store = function (username) {
    client.set(LAST_COUNTER, counter);
    client.set(KEY_PREFIX + String(counter++), username);
};


module.exports.take = function (callback) {
    client.get(KEY_PREFIX + String(taken), function (err, result) {
        if(err) throw err;
        else {
            if(result) {
                client.set(LAST_TAKEN, taken++);
            }
            callback(result);
        }
    });
};

module.exports.submitLastKey = function (lastKey) {
    client.set(LAST_KEY, lastKey);
};

module.exports.getLastSubmitedKey = function (callback) {
    client.get(LAST_KEY, function (err, key) {
        if(err) throw err;
        else {
            callback(key);
        }
    });
};