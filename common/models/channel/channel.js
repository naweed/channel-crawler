var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const CHANNEL_MODEL = 'channel';

var ChannelSchema = new Schema({
    channelID: {type: String},
    telegramID: {type: Number}
});

module.exports = mongoose.model(CHANNEL_MODEL, ChannelSchema);