var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const CHANNEL_MEMBER_MODEL = 'channelMember';

var ChannelMemberSchema = new Schema({
    channelID: {type: String, required: true},
    members: {type: Number, required: true}
});

module.exports = mongoose.model(CHANNEL_MEMBER_MODEL, ChannelMemberSchema);