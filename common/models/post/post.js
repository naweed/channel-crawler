var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const POST_MODEL = 'post';

var PostSchema = new Schema({
    text: {type: String},
    caption: {type: String},
    releasedTime: {type: Number},
    telegramID: {type: Number, required: true},
    channelID: {type: Number, required: true},
    channelName: {type: String}
});

module.exports = mongoose.model(POST_MODEL, PostSchema);