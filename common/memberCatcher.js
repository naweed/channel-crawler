var httpRequest = require('request');
var crawlerPersistance = require('./crawlerPersistance');
var ChannelMember = require('./models/channelMember/channelMember');

var log4js = require('log4js');
log4js.configure({
    appenders: {
        channelFinder: { type: 'file', filename: 'system.log' }
    },
    categories: {
        default: { appenders: [ 'channelFinder' ], level: 'debug' }
    }
});
var logger = log4js.getLogger();



const TELEGRAM_URI = 'https://t.me/';
const RESPONSE_OK = 200;


function htmlParser(body, callback) {
    var result;
    if(result = body.match(/([0-9\s]+) members/)) {
        callback(Number(result[1].replace(/\s/g,'')));
    } else {
        callback(null);
    }
}

function catchMember(channelUsername) {
    logger.info('requesting @' + channelUsername + ' from : ' + TELEGRAM_URI + channelUsername);
    httpRequest(TELEGRAM_URI + channelUsername, function (err, resp, body) {
        if(err)
            throw err;
        else if(resp.statusCode != RESPONSE_OK)
            // throw new Error('response status: ', resp.statusCode);
            logger.error('response from telegram was not ok. statusCode:' + resp.statusCode);
        else {
            htmlParser(body, function (members) {
                if(members){
                    new ChannelMember({
                        channelID: channelUsername,
                        members: members
                    }).save(function (err) {
                        if(err)
                            logger.error('could\'n save the new record with channelID: @'+ channelUsername + '\n error:' + err.toString());
                    });
                } else {
                    logger.warn('username @' + channelUsername +' doesnt belong to a valid channel!');
                }
            });
        }
    });
};


module.exports.usernameTaker = function () {

    crawlerPersistance.take(function (username) {
        if(username){
            catchMember(username);
        }
    });
};