var PostBatcher = require('./PostBatcher');
var memberCatcher = require('./memberCatcher');
var sync = require('synchronize');
var Set = require("collections/set");
var crawlerStore = require('./crawlerPersistance');
var Channel = require('./models/channel/channel');
var log4js = require('log4js');
var ChannelMembers = require('./models/channelMember/channelMember');
var persistant = require('./crawlerPersistance');

log4js.configure({
    appenders: {
        channelFinder: { type: 'file', filename: 'system.log' }
    },
    categories: {
        default: { appenders: [ 'channelFinder' ], level: 'debug' }
    }
});
var logger = log4js.getLogger();

const config = require('../config');
const POSTS_TO_ANALYSE_IN_EACH_CYCLE = 10000;


var channels = new Set();
var knownChannels = new Set();


function getPostCrawlingEntries(text) {
    var result;
    var set = new Set();
    if((result = text.match(/@([a-zA-Z0-9_]+)/g))) {
        result.forEach(function (result) {
            set.add(result.toLowerCase());
        });
    }
    if((result = text.match(/t.me\/joinchat\/([a-zA-Z0-9]+)/g))) {
        set.addEach(result);
    }
    return set;
}


function runCrawler(key) {
    var postBatcher = new PostBatcher('channelID text caption', POSTS_TO_ANALYSE_IN_EACH_CYCLE, key);

    sync(postBatcher, 'nextBatch');

    logger.info('member catcher thread starting with duration : ' + (config.telegram_requesting_timeout/1000));
    setInterval(memberCatcher.usernameTaker, config.telegram_requesting_timeout);

    logger.info('crawler is beginning to work ...');
    sync.fiber(function () {

        var i = 0;
        while(true) {
            logger.info(((++i) * POSTS_TO_ANALYSE_IN_EACH_CYCLE ) + ' posts analysed.');
            var posts;
            try {
                posts = postBatcher.nextBatch();
                if(posts.length <= 0)
                    break;
            } catch (err) {
                logger.error('problem while retrieving the next batch : '+ err.toString());
                throw err;
            }

            var usernames = new Set();
            posts.forEach(function (post) {
                if (post.caption)
                    usernames = usernames.union(getPostCrawlingEntries(post.caption));
                else
                    usernames = usernames.union(getPostCrawlingEntries(post.text));
            });

            usernames.forEach(function (username) {

                username = username.replace('@', '').replace('t.me/', '');
                if(channels.has(username) || knownChannels.has(username))
                    return;

                // console.log('new username : @'+ username, 'found!');
                channels.add(username);
                crawlerStore.store(username);

            });
        }

        console.log('*********************************************************');
        console.log('results:\n', channels.length, 'distinct usernames found!');
    });

};

function cachePreKnownChannels(callback) {
    Channel.find({}, 'channelID', null, function (err, results) {
        results.forEach(function (channel)  {
            knownChannels.add(channel.channelID.toLowerCase());
        });
        ChannelMembers.find({}, 'channelID', null, function (err, results) {
            results.forEach(function (channel) {
                knownChannels.add(channel.channelID.toLowerCase());
            });
            logger.info('pre known channels cached.');
            callback('data cached');
        });
    });
}

function start() {
    cachePreKnownChannels(function (result) {
        persistant.getLastSubmitedKey(function (key) {
            runCrawler(key);
        });
    });
}

module.exports.start = start;