module.exports = {
    dbUrl : 'mongodb://localhost/tootle',
    redisIndex: 2,
    telegram_requesting_timeout: 10000
};